# Study Garden

Study Garden is an educational platform where scholars can give lectures, hold
discussions with students, and seek funding from patrons.