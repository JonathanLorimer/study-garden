defmodule StudyGardenWeb.PageController do
  use StudyGardenWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
