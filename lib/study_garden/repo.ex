defmodule StudyGarden.Repo do
  use Ecto.Repo,
    otp_app: :study_garden,
    adapter: Ecto.Adapters.Postgres
end
